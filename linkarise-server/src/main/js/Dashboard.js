import React, { lazy, Suspense, useEffect } from 'react';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Context from './Store';

import CONSTANT from './Util/Constant';
import { getFetchHeader } from './Util';
import { TOGGLE_THEME } from './Util/Theme';
import StompClient from './Util/WebSocket';
import LocalStorage from './Util/LocalStorage';

import Sidebar from './HomePage/Sidebar';
import ThemeToggle from './Common/ThemeToggle';
import ErrorBoundary from './Common/ErrorBoundary';
import LoadingIndicator from './Common/LoadingIndicator';

const HomePageComponent = lazy(() => import('./HomePage'));

const useStyles = makeStyles((theme) => ({
  root: {
    flex: 1,
  },
  title: {
    flexGrow: 1,
  },
  text: {
    color: 'white',
  },
  appBar: {
    width: `calc(100% - ${CONSTANT.drawerWidth}px)`,
    marginLeft: CONSTANT.drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    marginLeft: CONSTANT.drawerWidth,
  },
}));

const AppRoute = (props) => {
  const classes = useStyles();

  const { session } = props;
  const { darkMode, dispatch } = React.useContext(Context);
  const isDarkMode = LocalStorage.getValueFromStorage(TOGGLE_THEME) === 'true' || darkMode;

  const onRecieveNewUserEvent = (message) => {
    const newUser = JSON.parse(message);
    console.log(newUser);
  };

  useEffect(() => {
    StompClient.register([
      { route: '/topic/newUser', callback: onRecieveNewUserEvent },
      { route: '/topic/updateUser', callback: (message) => { console.log('updateUser: ', message); } },
      { route: '/topic/deleteUser', callback: (message) => { console.log('deleteUser: ', message); } },
    ]);
  });

  const onSubmitClick = () => {
    const url = `${CONSTANT.baseURL}/app/logout`;
    const option = getFetchHeader('POST', session);
    console.log(option);
    console.log(session);
    fetch(url, option)
      .then((response) => {
        console.log(response);
        dispatch({ type: 'LOGOUT' });
      })
      .catch((err) => console.error(err));
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Linkarise App
          </Typography>
          <Button
            className={classes.text}
            variant="text"
            color="default"
            onClick={onSubmitClick}
          >
            Logout
          </Button>
          <ThemeToggle darkMode={isDarkMode} themeDispatch={dispatch} />
        </Toolbar>
      </AppBar>
      <Sidebar />

      <main className={classes.content}>
        <div className={classes.toolbar} />
        <ErrorBoundary>
          <Suspense fallback={<LoadingIndicator />}>
            <HomePageComponent />
          </Suspense>
        </ErrorBoundary>
      </main>
    </div>
  );
};

export default AppRoute;
