import React from 'react';

import InitialState from './State/ThemeState';

const Context = React.createContext(InitialState);

export default Context;
