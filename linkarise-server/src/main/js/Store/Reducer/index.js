export default function combineDispatch(dispatch) {
  return function (obj) {
    for (let i = 0; i < dispatch.length; i++) {
      dispatch[i](obj);
    }
  };
}
