import InitialState from '../State/SessionState';

import CONSTANT from '../../Util/Constant';
import LocalStorage from '../../Util/LocalStorage';

export const actionType = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

export default function sessionReducer(state, action) {
  switch (action.type) {
    case actionType.LOGIN:
      LocalStorage.setValueToStorage(CONSTANT.TOKEN_KEY, JSON.stringify(action.payload));
      return { ...state, session: action.payload };
    case actionType.LOGOUT:
      LocalStorage.removeValueFromStorage(CONSTANT.TOKEN_KEY);
      return InitialState;
    default:
      return state;
  }
}
