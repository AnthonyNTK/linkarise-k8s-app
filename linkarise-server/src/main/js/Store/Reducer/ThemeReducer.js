import { TOGGLE_THEME } from '../../Util/Theme';
import LocalStorage from '../../Util/LocalStorage';

export default function themeReducer(state, action) {
  switch (action.type) {
    case TOGGLE_THEME:
      const newDarkMode = !state.darkMode;
      LocalStorage.setValueToStorage(TOGGLE_THEME, newDarkMode);
      return { ...state, darkMode: newDarkMode };
    default:
      return state;
  }
}
