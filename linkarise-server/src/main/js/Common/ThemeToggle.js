import React from 'react';
import { Switch, FormControlLabel } from '@material-ui/core';

import { TOGGLE_THEME } from '../Util/Theme';

export default function ThemeToggle(props) {
  const { darkMode, themeDispatch } = props;

  const checked = darkMode || darkMode === 'true';

  return (
    <FormControlLabel
      label="Dark Theme"
      control={(
        <Switch
          checked={checked}
          onChange={() => themeDispatch({ type: TOGGLE_THEME })}
        />
      )}
    />
  );
}
