import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
  errorContainer: {
    flex: 1,
    display: 'flex',
    marginTop: 50,
  },
  error: {
    margin: 'auto',
  },
});

class ErrorBoundary extends React.Component {
  constructor() {
    super();

    this.state = {
      hasError: false,
    };
  }

  static getDerivedStateFromError(error) {
    console.log('error:', error);
    return {
      hasError: true,
    };
  }

  render() {
    if (this.state.hasError) {
      const { classes } = this.props;
      return (
        <div className={classes.errorContainer}>
          <h2 className={classes.error}>Not Found</h2>
        </div>
      );
    }
    return this.props.children;
  }
}

export default withStyles(styles)(ErrorBoundary);
