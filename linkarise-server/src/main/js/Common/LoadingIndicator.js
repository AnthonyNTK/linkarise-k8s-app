import React from 'react';
import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  circularProgressContainer: {
    flex: 1,
    display: 'flex',
    marginTop: 50,
  },
  circularProgress: {
    margin: 'auto',
  },
}));

export default function LoadingIndicator() {
  const classes = useStyles();

  return (
    <div className={classes.circularProgressContainer}>
      <CircularProgress className={classes.circularProgress} />
    </div>
  );
}
