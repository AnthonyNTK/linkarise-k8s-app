import SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

function register(registrations) {
  const socket = SockJS('/user');
  const stompClient = Stomp.over(socket);
	stompClient.debug = null
  stompClient.connect({}, (frame) => {
    registrations.forEach((registration) => {
      stompClient.subscribe(registration.route, registration.callback);
    });
  });
}

export default {
  register,
};
