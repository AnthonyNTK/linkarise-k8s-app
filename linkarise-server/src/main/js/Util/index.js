import CONSTANT from './Constant';
import LocalStorage from './LocalStorage';

export function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

export function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

export function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

export function getFetchHeader(method, session, data) {
  let credential = session;
  console.log('before', credential);
  if (credential == null || credential === undefined || credential === '') {
    credential = JSON.parse(LocalStorage.getValueFromStorage(CONSTANT.TOKEN_KEY));
  } else {
    credential = JSON.parse(credential);
  }
  console.log('after', credential);

  if (data == null || data === undefined) {
    return {
      method,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${credential.token}`,
      },
    };
  }

  return {
    method,
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${credential.token}`,
    },
  };
}
