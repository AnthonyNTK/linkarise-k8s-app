const CONSTANT = {
  drawerWidth: 240,
  userTablePadding: 50,
  serverURL: 'http://localhost:8080/api/v1',
  baseURL: 'http://localhost:8080',
  TOKEN_KEY: 'TOKEN_KEY',
};

export default CONSTANT;
