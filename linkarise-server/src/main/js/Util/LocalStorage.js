function getValueFromStorage(key) {
  try {
    const item = window.localStorage.getItem(key);
    return item;
  } catch (e) {
    console.log(e);
  }

  return false;
}

function setValueToStorage(key, value) {
  try {
    window.localStorage.setItem(key, value);
  } catch (e) {
    console.log(e);
  }
}

function removeValueFromStorage(key) {
  try {
    window.localStorage.removeItem(key);
  } catch (e) {
    console.log(e);
  }
}

export default {
  getValueFromStorage,
  setValueToStorage,
  removeValueFromStorage,
};
