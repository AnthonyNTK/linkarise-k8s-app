const darkTheme = {
  palette: {
    type: 'dark',
  },
};

const lighTheme = {
  palette: {
    type: 'light',
  },
};

export const TOGGLE_THEME = 'TOGGLE_THEME';

export default {
  darkTheme,
  lighTheme,
};
