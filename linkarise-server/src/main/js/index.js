import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

const pathname = window.location.search.substr(1);

ReactDOM.render(
  <React.StrictMode>
    <App initialPath={pathname} />
  </React.StrictMode>,
  document.getElementById('root'),
);
