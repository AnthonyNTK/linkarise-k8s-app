import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import CONSTANT from './Util/Constant';
import LocalStorage from './Util/LocalStorage';
import Theme, { TOGGLE_THEME } from './Util/Theme';

import Context from './Store';
import combineDispatch from './Store/Reducer';
import ThemeReducer from './Store/Reducer/ThemeReducer';
import SessionReducer from './Store/Reducer/SessionReducer';
import ThemeState from './Store/State/ThemeState';
import SessionState from './Store/State/SessionState';

import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import Dashboard from './Dashboard';

const App = (props) => {
  const { darkMode, session } = React.useContext(Context);
  const isDarkMode = LocalStorage.getValueFromStorage(TOGGLE_THEME) === 'true' || darkMode;
  const muiTheme = createMuiTheme(isDarkMode ? Theme.darkTheme : Theme.lighTheme);

  if (props.initialPath === 'register') {
    return (
      <RegisterPage />
    );
  }

  if (session == null || props.initialPath === 'login') {
    return (
      <LoginPage />
    );
  }

  return (
    <MuiThemeProvider theme={muiTheme}>
      <CssBaseline />
      <Switch>
        <Route path='/register' exact component={RegisterPage} />
        <Route path='/login' exact component={LoginPage} />
        <Route path='/' exact>
          <Dashboard session={session} />
        </Route>
      </Switch>
    </MuiThemeProvider>
  );
};

const ThemedApp = (props) => {
  const [themeState, themeDispatch] = React.useReducer(ThemeReducer, ThemeState);
  const [sessionState, sessionDispatch] = React.useReducer(SessionReducer, SessionState);

  let session = LocalStorage.getValueFromStorage(CONSTANT.TOKEN_KEY);
  if (session == null || session === 'null') session = sessionState.session;

  let initialPath = '';
  const initialPathParts = props.initialPath.split('=');
  if (initialPathParts.length === 2) {
    initialPath = initialPathParts[1];
  }

  return (
    <Context.Provider
      value={{
        darkMode: themeState.darkMode,
        session,
        dispatch: combineDispatch([themeDispatch, sessionDispatch]),
      }}
    >
      <Router>
        <App initialPath={initialPath} />
      </Router>
    </Context.Provider>
  );
};

export default ThemedApp;
