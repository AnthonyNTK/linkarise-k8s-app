package com.linkarise.k8sapp.general;

public class AppInfo {
	private String version;
	private String name;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
