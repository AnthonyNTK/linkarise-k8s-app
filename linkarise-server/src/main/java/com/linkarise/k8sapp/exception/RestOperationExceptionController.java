package com.linkarise.k8sapp.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.linkarise.linkarisecore.general.exception.ExceptionResponse;
import com.linkarise.linkarisecore.general.exception.RestOperationException;

@ControllerAdvice
public class RestOperationExceptionController extends ResponseEntityExceptionHandler {
	private static final Logger logger = LogManager.getLogger(RestOperationExceptionController.class);

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<ExceptionResponse> exception(Exception exception) {
		logger.error("Unknown Exception occured", exception);
	    return new ResponseEntity<ExceptionResponse>(new ExceptionResponse("Exception occured during runtime. For error detail, please check log."), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = RestOperationException.class)
	public ResponseEntity<ExceptionResponse> restException(RestOperationException exception) {
		String message = "Exception occured during REST Operation. For error detail, please check log.";
		if (exception.getMessage() != null && !exception.getMessage().isEmpty()) message = exception.getMessage();
	    return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(message), exception.getStatus());
	}
}
