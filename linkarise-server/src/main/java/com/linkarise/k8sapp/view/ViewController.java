package com.linkarise.k8sapp.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class ViewController {

	@RequestMapping(value = "/")
	public String index() {
		return "index";
	}
}