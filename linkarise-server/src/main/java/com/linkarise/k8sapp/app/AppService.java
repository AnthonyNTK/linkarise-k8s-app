package com.linkarise.k8sapp.app;

import org.springframework.http.HttpHeaders;

import com.linkarise.k8sapp.app.model.LoginInfo;
import com.linkarise.k8sapp.app.model.RegisterInfo;
import com.linkarise.k8sapp.app.model.UserInfo;
import com.linkarise.k8sapp.general.AppInfo;

public interface AppService {
	public AppInfo getAppInfo();

	public UserInfo getUserInfo(LoginInfo loginInfo, HttpHeaders httpHeaders);

	public UserInfo addUserInfo(RegisterInfo registerInfo, HttpHeaders httpHeaders);
}
