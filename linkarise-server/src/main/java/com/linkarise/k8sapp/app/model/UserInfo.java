package com.linkarise.k8sapp.app.model;

public class UserInfo {
	private String username;
	private String role;
	private String token;
	private String[] sessionId;

	public UserInfo() {}

	public UserInfo(String username, String role, String token, String[] sessionId) {
		this.username = username;
		this.role = role;
		this.token = token;
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String[] getSessionId() {
		return sessionId;
	}

	public void setSessionId(String[] sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "{" +
			" username='" + getUsername() + "'" +
			", role='" + getRole() + "'" +
			", token='" + getToken() + "'" +
			", sessionId='" + getSessionId() + "'" +
			"}";
	}
}
