package com.linkarise.k8sapp.app.impl;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.reflect.FieldUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.linkarise.linkarisecore.general.exception.RestOperationException;
import com.linkarise.linkarisecore.general.model.Response;
import com.linkarise.linkarisecore.util.ConstantUtil;
import com.linkarise.linkarisecore.util.ParamUtil;
import com.linkarise.k8sapp.app.UserService;
import com.linkarise.k8sapp.datamodel.User;
import com.linkarise.k8sapp.repo.UserRepository;

@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepo;

    @Cacheable(ConstantUtil.USER_LIST_CACHE_KEY)
    @Override
    public List<User> getAllUser() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {

        }
        return userRepo.findAll();
    }

    @Override
    public Response createUser(User data) {
        User savedUser = userRepo.insert(data);
        if (savedUser != null)
            return new Response();
        else
            throw new RestOperationException("Failed to create user.", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Cacheable(ConstantUtil.User_CACHE_KEY)
    @Override
    public User readUser(String id) {
        User user = userRepo.findOne(id);
        if (user != null)
            return user;
        else
            throw new RestOperationException("Failed to find user.", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    public User updateUser(String id, Map<String, Object> payload) throws IllegalAccessException {
        User originalUser = userRepo.findOne(id);
        if (originalUser != null) {
            for (String key : payload.keySet()) {
                String value = (String) payload.get(key);
                if (!ParamUtil.isNullOrEmpty(value))
                    updateParamter(key, value, originalUser);
            }

            User user = userRepo.save(originalUser);
            if (user != null)
                return user;
            else
                throw new RestOperationException("Failed to update user.", HttpStatus.INTERNAL_SERVER_ERROR);
        } else
            throw new RestOperationException("Failed to update User.");
    }

    @Override
    public Response deleteUser(String id) {
        userRepo.delete(id);
        return new Response();
    }

    private void updateParamter(String key, String value, User target) throws IllegalAccessException {
        Field privateField = FieldUtils.getField(User.class, key, true);
        FieldUtils.writeField(privateField, target, value);
    }
}
