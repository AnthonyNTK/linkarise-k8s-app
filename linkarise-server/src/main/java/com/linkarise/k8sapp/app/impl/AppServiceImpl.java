package com.linkarise.k8sapp.app.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import com.linkarise.k8sapp.app.AppService;
import com.linkarise.k8sapp.app.model.LoginInfo;
import com.linkarise.k8sapp.app.model.RegisterInfo;
import com.linkarise.k8sapp.app.model.UserInfo;
import com.linkarise.k8sapp.datamodel.User;
import com.linkarise.k8sapp.general.AppInfo;
import com.linkarise.k8sapp.repo.UserRepository;
import com.linkarise.linkarisecore.general.exception.RestOperationException;

@Component
public class AppServiceImpl implements AppService {
	private static final Logger logger = LogManager.getLogger(AppServiceImpl.class);

	@Value("${spring.application.clientId}")
	private String clientId;

	@Value("${spring.application.clientSecret}")
	private String clientSecret;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private TokenEndpoint tokenEndpoint;

	@Override
	public AppInfo getAppInfo() {
		AppInfo appInfo = new AppInfo();
		appInfo.setName("Test");
		appInfo.setVersion("0.0.1");

		return appInfo;
	}

	@Override
	public UserInfo getUserInfo(LoginInfo loginInfo, HttpHeaders httpHeaders) {
		String username = loginInfo.getUsername();

		String[] sessionId = getCookies(httpHeaders);

		User user = userRepo.findByUsername(username);
		String token = getAccessToken(user.getUsername(), loginInfo.getPassword(), user.getRole());
		UserInfo userInfo = new UserInfo(username, user.getRole(), token, sessionId);
		return userInfo;
	}

	@Override
	public UserInfo addUserInfo(RegisterInfo registerInfo, HttpHeaders requestHeaders) {
		String username = registerInfo.getUsername();
		String originalPassword = registerInfo.getPassword();
		registerInfo.setPassword(passwordEncoder.encode(originalPassword));

		User newUser = new User(registerInfo);
		userRepo.save(newUser);

		String token = getAccessToken(newUser.getUsername(), originalPassword, newUser.getRole());

		String[] sessionId = getCookies(requestHeaders);

		UserInfo userInfo = new UserInfo(username, newUser.getRole(), token, sessionId);
		return userInfo;
	}

	private String getAccessToken(String username, String password, String role) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("client_id", clientId);
		parameters.put("client_secret", clientSecret);
		parameters.put("username", username);
		parameters.put("password", password);
		parameters.put("grant_type", "password");

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(role));

		// Create principal and auth token
		org.springframework.security.core.userdetails.User userPrincipal = new org.springframework.security.core.userdetails.User(
				clientId, clientSecret, true, true, true, true, authorities);

		UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken(userPrincipal, clientId,
				authorities);

		try {
			ResponseEntity<OAuth2AccessToken> accessToken = tokenEndpoint.postAccessToken(principal, parameters);
			return accessToken.getBody().getValue();
		} catch (HttpRequestMethodNotSupportedException e) {
			logger.error("Failed to get access token: ", e);
			throw new RestOperationException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private String[] getCookies(HttpHeaders httpHeaders) {
		String[] sessionId = new String[1];

		if (Objects.nonNull(httpHeaders.get("cookie"))) {
			sessionId = httpHeaders.get("cookie").get(0).split(";");
		} else {
			logger.info("Cookie not found. JSessionId not set.");

			/**
			 * Swagger2 does not support cookie for that putting default sessssion id.
			 */
			sessionId[0] = "JSEESION-ID";
		}
		return sessionId;
	}
}
