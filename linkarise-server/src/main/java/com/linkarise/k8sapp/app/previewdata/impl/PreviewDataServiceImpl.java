package com.linkarise.k8sapp.app.previewdata.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.linkarise.k8sapp.app.previewdata.FirebaseAdapter;
import com.linkarise.k8sapp.app.previewdata.PreviewDataService;
import com.linkarise.k8sapp.app.previewdata.model.PreviewData;
import com.linkarise.k8sapp.app.previewdata.model.PreviewDataRequest;
import com.linkarise.k8sapp.app.previewdata.model.PreviewDataResponse;
import com.linkarise.linkarisecore.general.exception.RestOperationException;

@Component
public class PreviewDataServiceImpl implements PreviewDataService {
	private static final Logger logger = LogManager.getLogger(PreviewDataServiceImpl.class);

	@Autowired
	private FirebaseAdapter firebaseAdapter;

	@Override
	public PreviewDataResponse getPreviewData(PreviewDataRequest request) {
		try {
			List<PreviewData> previewData = firebaseAdapter.getPreviewData();
			return new PreviewDataResponse(previewData);
		} catch (ExecutionException | InterruptedException e) {
			logger.error("Failed to query data from firebase", e);

			throw new RestOperationException();
		}
	}
}
