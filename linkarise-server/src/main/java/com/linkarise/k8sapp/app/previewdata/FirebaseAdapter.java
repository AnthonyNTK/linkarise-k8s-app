package com.linkarise.k8sapp.app.previewdata;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import com.linkarise.k8sapp.app.impl.AppServiceImpl;
import com.linkarise.k8sapp.app.previewdata.model.PreviewData;

@Component
public class FirebaseAdapter {
	private static final Logger logger = LogManager.getLogger(AppServiceImpl.class);

	private FirebaseApp app;

	public FirebaseAdapter() {
		try {
			ClassPathResource resource = new ClassPathResource("service-account.json");
			GoogleCredentials credentials = GoogleCredentials.fromStream(resource.getInputStream());

			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(credentials)
					.setDatabaseUrl("https://flutter-linkarise.firebaseio.com/").build();

			app = FirebaseApp.initializeApp(options);

			logger.info("Firebase App initialized, name: {}", app.getName());
		} catch (IOException e) {
			logger.info("Firebase App failed to initialize.", e);

			app = FirebaseApp.initializeApp();
		}
	}

	public Firestore getFirestore() {
		return FirestoreOptions.getDefaultInstance().getService();
	}

	public List<PreviewData> getPreviewData() throws ExecutionException, InterruptedException {
		ApiFuture<QuerySnapshot> query = getFirestore().collection("link").limit(10).get();
		QuerySnapshot querySnapshot = query.get();
		List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
		List<PreviewData> dataList = documents.stream().map((document) -> document.toObject(PreviewData.class))
				.collect(Collectors.toList());
		return dataList;
	}
}
