package com.linkarise.k8sapp.app.previewdata.model;

import java.util.List;

import com.linkarise.linkarisecore.general.model.Response;

public class PreviewDataResponse extends Response {
	private List<PreviewData> previewData;

	public PreviewDataResponse() {
	}

	public PreviewDataResponse(List<PreviewData> previewData) {
		this.previewData = previewData;
	}

	public List<PreviewData> getPreviewData() {
		return previewData;
	}

	public void setPreviewData(List<PreviewData> previewData) {
		this.previewData = previewData;
	}
}
