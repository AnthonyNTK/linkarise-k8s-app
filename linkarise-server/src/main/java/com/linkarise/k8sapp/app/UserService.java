package com.linkarise.k8sapp.app;

import java.util.List;
import java.util.Map;

import com.linkarise.k8sapp.datamodel.User;
import com.linkarise.linkarisecore.general.model.Response;

public interface UserService {
    public List<User> getAllUser();

    public Response createUser(User data);

    public User readUser(String id);

    public User updateUser(String id, Map<String, Object> payload) throws IllegalAccessException;

    public Response deleteUser(String id);
}