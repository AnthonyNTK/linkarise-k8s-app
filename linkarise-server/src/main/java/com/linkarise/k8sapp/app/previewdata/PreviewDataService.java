package com.linkarise.k8sapp.app.previewdata;

import com.linkarise.k8sapp.app.previewdata.model.PreviewDataRequest;
import com.linkarise.k8sapp.app.previewdata.model.PreviewDataResponse;

public interface PreviewDataService {
	public PreviewDataResponse getPreviewData(PreviewDataRequest request);
}
