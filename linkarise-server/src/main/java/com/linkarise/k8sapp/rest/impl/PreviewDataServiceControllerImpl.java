package com.linkarise.k8sapp.rest.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkarise.k8sapp.app.previewdata.PreviewDataService;
import com.linkarise.k8sapp.app.previewdata.model.PreviewData;
import com.linkarise.k8sapp.app.previewdata.model.PreviewDataRequest;
import com.linkarise.k8sapp.app.previewdata.model.PreviewDataResponse;
import com.linkarise.k8sapp.rest.PreviewDataServiceController;
import com.linkarise.linkarisecore.general.exception.RestOperationException;
import com.linkarise.linkarisecore.general.model.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("${spring.rest.prefix}/v1/previewdata")
@Api(value = "${spring.rest.prefix}/v1/previewdata", description = "PreviewData.v1", tags = "Preview Data")
public class PreviewDataServiceControllerImpl implements PreviewDataServiceController {
	private static final int SIZE_LIMIT = 100;

	@Autowired
	private PreviewDataService previewDataService;

	@Override
	@ApiOperation(httpMethod = "GET", value = "Get preview data", nickname = "getPreviewData", notes = "Get a list of preview data")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@RequestMapping(value = "/getPreviewData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public PreviewDataResponse getPreviewData(
			@ApiParam(value = "page", required = false) @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@ApiParam(value = "size", required = true) @RequestParam(value = "size", required = true, defaultValue = "10") Integer size,
			@ApiParam(value = "createdAt", required = false) @RequestParam(value = "createdAt", required = false, defaultValue = "") String createdAt,
			@ApiParam(value = "tags", required = false) @RequestParam(value = "tags", required = false, defaultValue = "") List<String> tags) {

		if (size.intValue() > SIZE_LIMIT)
			throw new RestOperationException("Maximum size limit is 100");
		if (page.intValue() <= 0)
			throw new RestOperationException("Minimum page number must be greater than zero");

		PreviewDataRequest request = new PreviewDataRequest();
		request.setPage(page.intValue());
		request.setSize(size.intValue());
		request.setTags(tags);
		request.setCreatedAt(createdAt);
		return previewDataService.getPreviewData(request);
	}

	@Override
	@ApiOperation(httpMethod = "POST", value = "Create preview data", nickname = "createPreviewData", notes = "Create a preview data")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Response create(@RequestBody PreviewData data) {
		return null;
	}

	@Override
	@ApiOperation(httpMethod = "GET", value = "Read preview data", nickname = "readPreviewData", notes = "Read a preview data")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PreviewData read(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id) {
		return null;
	}

	@Override
	@ApiOperation(httpMethod = "PUT", value = "Update preview data", nickname = "updatePreviewData", notes = "Update a preview data")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PreviewData update(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@ApiParam(value = "payload", required = true) @RequestBody(required = true) Map<String, Object> payload) {
		return null;
	}

	@Override
	@ApiOperation(httpMethod = "DELETE", value = "Delete preview data", nickname = "deletePreviewData", notes = "Delete a preview data")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response delete(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id) {
		return null;
	}
}
