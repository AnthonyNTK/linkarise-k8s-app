package com.linkarise.k8sapp.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;

import com.linkarise.k8sapp.app.model.LoginInfo;
import com.linkarise.k8sapp.app.model.RegisterInfo;
import com.linkarise.k8sapp.app.model.UserInfo;
import com.linkarise.k8sapp.general.AppInfo;

public interface AppServiceController {
	public AppInfo getAppInfo();
	public UserInfo login(HttpHeaders httpHeaders, LoginInfo loginInfo);
	public UserInfo register(HttpHeaders httpHeaders, RegisterInfo registerInfo);
	public void logout(HttpServletRequest request);
}
