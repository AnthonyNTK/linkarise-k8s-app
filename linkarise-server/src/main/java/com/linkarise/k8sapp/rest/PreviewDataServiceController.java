package com.linkarise.k8sapp.rest;

import java.util.List;

import com.linkarise.k8sapp.app.previewdata.model.PreviewData;
import com.linkarise.k8sapp.app.previewdata.model.PreviewDataResponse;
import com.linkarise.linkarisecore.general.controller.BaseController;

public interface PreviewDataServiceController extends BaseController<PreviewData> {
	public PreviewDataResponse getPreviewData(Integer page, Integer size, String createdAt, List<String> tags);
}
