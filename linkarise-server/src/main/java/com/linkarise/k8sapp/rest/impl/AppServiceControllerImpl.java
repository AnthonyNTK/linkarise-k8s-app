package com.linkarise.k8sapp.rest.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

import com.linkarise.linkarisecore.general.exception.RestOperationException;
import com.linkarise.linkarisecore.util.ParamUtil;
import com.linkarise.k8sapp.app.AppService;
import com.linkarise.k8sapp.app.model.LoginInfo;
import com.linkarise.k8sapp.app.model.RegisterInfo;
import com.linkarise.k8sapp.app.model.UserInfo;
import com.linkarise.k8sapp.general.AppInfo;
import com.linkarise.k8sapp.rest.AppServiceController;

@RestController
@RequestMapping("/app")
@Api(value = "/app", description = "App", tags = "App Service")
public class AppServiceControllerImpl implements AppServiceController {
	@Autowired
	private AppService appService;

	@Autowired
    private TokenStore tokenStore;

	@Override
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@ApiOperation(httpMethod = "GET", value = "Get all app info", response = AppInfo.class, nickname = "getAppInfo", notes = "Get a list of information of install app(s)")
	@RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AppInfo getAppInfo() {
		return appService.getAppInfo();
	}

	@Override
	@ApiOperation(value = "Authenticated User Login", response = UserInfo.class)
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo login(@RequestHeader HttpHeaders httpHeaders, @RequestBody(required=true) LoginInfo loginInfo) {
		return appService.getUserInfo(loginInfo, httpHeaders);
    }

	@Override
	@ApiOperation(value = "Authenticated User Login", response = UserInfo.class)
    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo register(@RequestHeader HttpHeaders httpHeaders, @RequestBody(required=true) RegisterInfo registerInfo) {
		if (ParamUtil.isNullOrEmpty(registerInfo.getFirstName(), registerInfo.getLastName())) {
			throw new RestOperationException("First name and last name cannot be both null or empty");
		}

		if (ParamUtil.isNullOrEmpty(registerInfo.getUsername())) {
			throw new RestOperationException("Username cannot be null or empty");
		}

		if (ParamUtil.isNullOrEmpty(registerInfo.getPassword())) {
			throw new RestOperationException("Password cannot be null or empty");	
		}

		return appService.addUserInfo(registerInfo, httpHeaders);
    }

	@Override
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@ApiOperation(value = "User Logout")
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        String tokenValue = authHeader.replace("Bearer", "").trim();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        tokenStore.removeAccessToken(accessToken);
    }
}
