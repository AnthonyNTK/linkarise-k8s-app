package com.linkarise.k8sapp.rest.impl;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.linkarise.k8sapp.app.UserService;
import com.linkarise.k8sapp.datamodel.User;
import com.linkarise.k8sapp.rest.UserController;
import com.linkarise.linkarisecore.general.exception.RestOperationException;
import com.linkarise.linkarisecore.general.model.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("${spring.rest.prefix}/v1/user")
@Api(value = "${spring.rest.prefix}/v1/user", description = "User.v1", tags = "User")
public class UserControllerImpl implements UserController {
	private static final Logger logger = LogManager.getLogger(UserControllerImpl.class);

	@Autowired
	private UserService userService;

	@Override
	@ApiOperation(httpMethod = "GET", value = "Get user", nickname = "getUser", notes = "Get a list of user")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@RequestMapping(value = "/getAllUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUser() {
		try {
			return userService.getAllUser();
		} catch (DataAccessException e) {
			logger.error("Failed to retrieve list of user.", e);
			throw new RestOperationException("Failed to retrieve list of user.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@ApiOperation(httpMethod = "POST", value = "Create user", nickname = "createUser", notes = "Create a user")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Response create(@RequestBody User data) {
		try {
			return userService.createUser(data);
		} catch (DataAccessException e) {
			logger.error("Failed to create user.", e);
			throw new RestOperationException("Failed to create user.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@ApiOperation(httpMethod = "GET", value = "Read user", nickname = "readUser", notes = "Read a user")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User read(@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id) {
		try {
			return userService.readUser(id);
		} catch (DataAccessException e) {
			logger.error("Failed to find user of id [{}].", id, e);
			throw new RestOperationException("Failed to find user.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@ApiOperation(httpMethod = "PUT", value = "Update user", nickname = "updateUser", notes = "Update a user")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User update(@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@ApiParam(value = "payload", required = true) @RequestBody(required = true) Map<String, Object> payload) {
		try {
			return userService.updateUser(id, payload);
		} catch (DataAccessException | IllegalAccessException e) {
			logger.error("Failed to update user of id [{}].", id, e);
			throw new RestOperationException("Failed to update user.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@ApiOperation(httpMethod = "DELETE", value = "Delete user", nickname = "deleteUser", notes = "Delete a user")
	@ApiImplicitParam(name = "Authorization", value = "OAuth 2.0 Authorization Bearer Token", dataType = "string", paramType = "header")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response delete(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id) {
		try {
			return userService.deleteUser(id);
		} catch (DataAccessException e) {
			logger.error("Failed to delete user.", e);
			throw new RestOperationException("Failed to delete user.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
