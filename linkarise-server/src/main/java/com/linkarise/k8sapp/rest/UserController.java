package com.linkarise.k8sapp.rest;

import java.util.List;

import com.linkarise.k8sapp.datamodel.User;
import com.linkarise.linkarisecore.general.controller.BaseController;

public interface UserController extends BaseController<User> {
	public List<User> getAllUser();
}
