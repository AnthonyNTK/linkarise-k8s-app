package com.linkarise.k8sapp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.linkarise.k8sapp.datamodel.User;
import com.linkarise.k8sapp.repo.UserRepository;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class K8sAppApplication extends SpringBootServletInitializer implements CommandLineRunner {
    private static final Logger logger = LogManager.getLogger(K8sAppApplication.class);

    @Autowired
    private UserRepository userRepo;

    @Value("${spring.application.admin.username}")
    private String aUsername;

    @Value("${spring.application.admin.password}")
    private String aPassword;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Cleaning up old user records......");
        userRepo.deleteAll();

        logger.info("Initializing admin user records......");
        userRepo.save(new User(aUsername, passwordEncoder().encode(aPassword), "ADMIN, ACTUATOR"));
        logger.info("Admin user record initialized.");
    }

    public static void main(String[] args) {
        SpringApplication.run(K8sAppApplication.class, args);
    }
}
