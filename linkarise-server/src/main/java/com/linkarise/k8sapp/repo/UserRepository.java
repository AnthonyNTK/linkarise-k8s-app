package com.linkarise.k8sapp.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.linkarise.k8sapp.datamodel.User;

public interface UserRepository extends MongoRepository<User, String> {
	public List<User> findByFirstName(String firstName);
	public List<User> findByLastName(String lastName);
	public List<User> findByRole(String role);
	public User findByUsername(String username);
}
