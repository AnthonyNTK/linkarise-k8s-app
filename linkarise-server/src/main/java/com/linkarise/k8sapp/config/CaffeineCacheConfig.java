package com.linkarise.k8sapp.config;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import com.linkarise.linkarisecore.util.ConstantUtil;

@EnableCaching
@Configuration
public class CaffeineCacheConfig {
    private static final Logger logger = LogManager.getLogger(CaffeineCacheConfig.class);

    @Bean
    public KeyGenerator createMapperKeyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object target, Method method, Object... params) {
                String className = method.getDeclaringClass().getName();
                String methodName = method.getName();
                String paramHash = String.valueOf(Arrays.toString(params).hashCode());
                String cacheKey = new StringJoiner("_").add(className).add(methodName).add(paramHash).toString();

                return cacheKey;
            }
        };
    }

    @Bean
    public CacheManager createCacheManager() {
        logger.info("Cache manager initializing ...");

        LoadingCache<Object, Object> userListCache = Caffeine.newBuilder().maximumSize(100)
                .refreshAfterWrite(5, TimeUnit.MINUTES).build(key -> load(key));

        LoadingCache<Object, Object> userCache = Caffeine.newBuilder().maximumSize(10_000)
                .expireAfterWrite(1, TimeUnit.HOURS).build(key -> load(key));

        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new CaffeineCache(ConstantUtil.USER_LIST_CACHE_KEY, userListCache),
                new CaffeineCache(ConstantUtil.User_CACHE_KEY, userCache)));
        return cacheManager;
    }

    private Object load(Object key) throws Exception {
        return null;
    }
}