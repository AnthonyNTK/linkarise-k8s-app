package com.linkarise.k8sapp.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.linkarise.k8sapp.datamodel.User;

@Component
public class DataEventHandler extends AbstractMongoEventListener<User> {
    private static final Logger logger = LogManager.getLogger(DataEventHandler.class);

    @Autowired
    private SimpMessagingTemplate websocket;

    @Override
    public void onAfterSave(AfterSaveEvent<User> event) {
        logger.info("Event [{}] triggered.", event.getClass());

        websocket.convertAndSend(WebSocketConfig.MESSAGE_PREFIX + "/newUser", event.getSource());
    }
}
