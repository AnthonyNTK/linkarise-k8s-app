package com.linkarise.k8sapp.config;

import java.io.PrintWriter;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.linkarise.linkarisecore.general.exception.ExceptionResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.ConsumptionProbe;
import io.github.bucket4j.Refill;

public class RateLimitInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LogManager.getLogger(RateLimitInterceptor.class);

    private final Map<String, Bucket> buckets = new ConcurrentHashMap<>();

    private Gson gson = new Gson();

    private static Bucket bucket() {
        Refill refill = Refill.intervally(60, Duration.ofMinutes(1));
        Bandwidth limit = Bandwidth.classic(60, refill).withInitialTokens(60);
        Bucket bucket = Bucket4j.builder().addLimit(limit).build();
        return bucket;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        logger.info("REST API Request: [{} {}]", request.getMethod(), request.getRequestURI());

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
            ipAddress = ipAddress.contains(",") ? ipAddress.split(",")[0] : ipAddress;
        }

        Bucket requestBucket = this.buckets.computeIfAbsent(ipAddress, key -> bucket());

        ConsumptionProbe probe = requestBucket.tryConsumeAndReturnRemaining(1);
        if (probe.isConsumed()) {
            response.addHeader("X-Rate-Limit-Remaining", Long.toString(probe.getRemainingTokens()));
            return true;
        }

        response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
        response.addHeader("X-Rate-Limit-Retry-After-Milliseconds",
                Long.toString(TimeUnit.NANOSECONDS.toMillis(probe.getNanosToWaitForRefill())));

        ExceptionResponse exception = new ExceptionResponse("Rate limit hitted");
        String exceptionString = gson.toJson(exception);

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(exceptionString);
        out.flush();

        logger.info("Rate limit hitted for endpoint: [{}]", request.getRequestURI());

        return false;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception ex) {
        logger.info("REST API Response: [{} {}] {}", request.getMethod(), request.getRequestURI(),
                response.getStatus());
    }
}
