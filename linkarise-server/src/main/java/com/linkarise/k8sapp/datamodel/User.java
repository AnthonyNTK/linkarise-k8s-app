package com.linkarise.k8sapp.datamodel;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.linkarise.k8sapp.app.model.RegisterInfo;

@Document(collection = "user")
@TypeAlias("user")
public class User {
	@Id
	public String id;

	private String username;
	private String firstName;
	private String lastName;
	private String password;
	private String role;

	public User(RegisterInfo registerInfo) {
		firstName = registerInfo.getFirstName();
		lastName = registerInfo.getLastName();
		password = registerInfo.getPassword();
		username = registerInfo.getUsername();
		role = "NORMAL_USER";
	}

	public User(String username, String password, String role) {
		this.username = username;
		this.password = password;
		this.firstName = "";
		this.lastName = "";
		this.role = role;
	}

	@PersistenceConstructor
	public User(String username, String password, String firstName, String lastName, String role) {
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return String.format("Customer[id=%s, username=%s name='%s %s']", id, username, firstName, lastName);
	}
}
