package com.linkarise.linkarisecore.util;

public class ConstantUtil {
	public static final String[] USAGE_DATA_HEADER = { "AccountOwnerId", "AccountName", "ServiceAdministratorId",
			"SubscriptionId", "SubscriptionGuid", "SubscriptionName", "Date", "Month", "Day", "Year", "Product",
			"MeterID", "MeterCategory", "MeterSubCategory", "MeterRegion", "MeterName", "ConsumedQuantity",
			"ResourceRate", "ExtendedCost", "ResourceLocation", "ConsumedService", "InstanceID", "ServiceInfo1",
			"ServiceInfo2", "AdditionalInfo", "Tags", "StoreServiceIdentifier", "DepartmentName", "CostCenter",
			"UnitOfMeasure", "ResourceGroup", "ChargesBilledSeparately" };
	public static final String[] USAGE_DATA_IGNORE_FIELDS = { "ServiceAdministratorId", "SubscriptionId", "Date",
			"Year", "Month", "Day", "ServiceInfo1", "ServiceInfo2", "AdditionalInfo", "ChargesBilledSeparately",
			"StoreServiceIdentifier", "DepartmentName", "CostCenter", "MeterRegion", "ConsumedService",
			"MeterSubCategory", "InstanceID", "Tags" };
	public static final String USER_LIST_CACHE_KEY = "userListCache";
	public static final String User_CACHE_KEY = "userCache";
	public static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/resources/", "classpath:/static/",
			"classpath:/public/" };
}