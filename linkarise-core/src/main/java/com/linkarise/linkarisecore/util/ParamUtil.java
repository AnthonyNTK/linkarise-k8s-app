package com.linkarise.linkarisecore.util;

public class ParamUtil {
    public static boolean isNullOrEmpty(String param) {
		return param == null || param.isEmpty();
	}

	public static boolean isNullOrEmpty(String... params) {
		boolean isNullOrEmpty = false;
		for (String param : params) {
			if (param == null || param.isEmpty()) {
				isNullOrEmpty = true;
				break;
			}
		}

		return isNullOrEmpty;
	}
}