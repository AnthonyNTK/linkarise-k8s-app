package com.linkarise.linkarisecore.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommonUtil {
	public static String[] removeStringFromArray(String[] array, String... words) {
		List<String> list = new ArrayList<String>(Arrays.asList(array));
		for (String word : words)
			list.remove(word);

		return list.toArray(new String[list.size()]);
	}

	public static int getIndexFromArray(String[] array, String word) {
		List<String> list = new ArrayList<String>(Arrays.asList(array));
		return list.indexOf(word);
	}
}
