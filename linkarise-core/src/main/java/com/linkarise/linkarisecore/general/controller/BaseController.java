package com.linkarise.linkarisecore.general.controller;

import java.util.Map;

import com.linkarise.linkarisecore.general.model.Response;

public interface BaseController<T> {
	public Response create(T t);
	public T read(String id);
	public T update(String id, Map<String, Object> payload);
	public Response delete(String id);
}
