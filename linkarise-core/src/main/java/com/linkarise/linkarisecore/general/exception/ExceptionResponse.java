package com.linkarise.linkarisecore.general.exception;

import com.linkarise.linkarisecore.general.model.Response;

public class ExceptionResponse extends Response {
    private String message;

    public ExceptionResponse(String message) {
    	super(false);
        this.message = message;
    }
}
