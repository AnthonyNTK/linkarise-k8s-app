package com.linkarise.linkarisecore.general.model;

public class Response {
	private boolean result;

	public Response() {
		this.result = true;
	}

	public Response(boolean result) {
		this.result = result;
	}
}
