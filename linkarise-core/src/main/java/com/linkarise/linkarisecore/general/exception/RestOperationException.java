package com.linkarise.linkarisecore.general.exception;

import org.springframework.http.HttpStatus;

public class RestOperationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private HttpStatus status;
	
	private String message;

	public RestOperationException() {
		this.status = HttpStatus.BAD_REQUEST;
	}

	public RestOperationException(String message) {
		this.status = HttpStatus.BAD_REQUEST;
		this.message = message;
	}

	public RestOperationException(HttpStatus status) {
		this.status = status;
	}

	public RestOperationException(String message, HttpStatus status) {
		this.status = status;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
