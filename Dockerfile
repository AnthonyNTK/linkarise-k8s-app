FROM openjdk:8-jre
ARG JAR_FILE
COPY ${JAR_FILE} k8s-app.war

ENTRYPOINT ["java","-jar","/k8s-app.war"]