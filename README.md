Linkarise-K8s-App

## Dependencies version
1. Node.js 10.15.0
2. NPM 6.13.4
3. Docker Desktop 2.2.0.4
4. Docker 19.03.8
5. Minikube 1.9.2
6. Kubectl 1.5
7. Maven 3.6.3
8. Java 8

## Tech / Scheme / Approach / Framework
1. Spring boot
2. Spring Security
3. Resource Owner Password Credentials (ROPC) Authentication
4. OAuth
5. JSON Web Token (JWT)
6. Swagger
7. Mongo JPA
8. Log4j2
9. Static SPA with React

## To run this project
1. Install dependencies, `mvn install`
2. Update your own `service-account.json`
3. Run the project, `mvn spring-boot:run`

## Available command
### `mvn clean`
Clear all content under `target` folder

### `mvn eclipse:clean`
Clear `.classpath`, `.setting`, `.factorypath` (eclipse-related generated file)

### `mvn eclipse:eclipse`
Build `.classpath` and related files for package and compile

### `mvn package`
Package project as `war` distributed file which can be ran using `java -jar {package-name}.war`

### `mvn docker:build`
Build docker image which will include the project executable as entry point, the image will be tagged as `linkarise/k8s-app` with `latest` in local repository

## Docker related command
### `docker run linkarise/k8s-app`
Spin up local docker images, usually run with `-p 8080:8080`, forward host request from port 8080 to container's port 8080 which the application server listening to. With `--name {custom-name}`, you can specify the container name when the images was being loaded

### `docker tag linkarise/l8s-ap {docker-id}/{image-name}:{version}`
Change the name of the docker images, strictly follow the above format suggested by Docker Hub

### `docker push {docker-id}/{image-name}:{version}`
Push to docker image to Docker Hub Registry

### `docker stop {container-name}/{container-id}`
Stop the container, `container-name` can be specified in `run` stage. It and`container-id` can be both found using `docker ps`

### `docker rm {container-name}/{container-id}`
Remove the container

### `docker system prune`
Delete unused images, container and related resource gracefully

## K8s related command
### `minikube start --vm-driver=hyperv/virtualbox/docker`
Start local K8s Cluster specifying hypervisor, default `hyperv` for `Docker Desktop for Windows`

### `minikube stop`
Stop local K8s Cluster

### `minikube status`
Check local K8s Cluster status`

### `minikube service {app-name}`
Print out running service in K8s cluster of `default` namespace. It will attempt to open default browser and lanuch the application if it listens to port 80 at the host system.

### `kubectl apply -f {directory/file}`
Execute all `.yaml/.yml` file(s) to create the resources specified

### `kubectl get pods`
Get status of running pods

### `kubectl get services`
Get information of running servies

### `kubectl cluster-info`
Get information of running K8s Cluster