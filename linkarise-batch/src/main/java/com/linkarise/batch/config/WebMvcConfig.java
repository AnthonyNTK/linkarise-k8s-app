package com.linkarise.batch.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.async.CallableProcessingInterceptor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.linkarise.linkarisecore.util.ConstantUtil;

@EnableWebMvc
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {
	@Autowired
	private CallableProcessingInterceptor callableProcessingInterceptor;

	@Autowired
	private AsyncTaskExecutor asyncTaskExecutor;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Override
	public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
		configurer.setDefaultTimeout(360000).setTaskExecutor(asyncTaskExecutor);
		configurer.registerCallableInterceptors(callableProcessingInterceptor);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations(ConstantUtil.CLASSPATH_RESOURCE_LOCATIONS);
	}
}
