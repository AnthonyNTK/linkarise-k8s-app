package com.linkarise.batch.model.batch;

public class UsageData {
    private String AccountOwnerId;
    private String AccountName;
    private String SubscriptionGuid;
    private String SubscriptionName;
    private String Product;
    private String MeterID;
    private String MeterCategory;
    private String MeterName;
    private String ConsumedQuantity;
    private String ResourceRate;
    private String ExtendedCost;
    private String ResourceLocation;
    private String UnitOfMeasure;
    private String ResourceGroup;

    public UsageData() {
    }

    public UsageData(String AccountOwnerId, String AccountName, String ServiceAdministratorId, String SubscriptionId, String SubscriptionGuid, String SubscriptionName, String Month, String Day, String Year, String Product, String MeterID, String MeterCategory, String MeterSubCategory, String MeterRegion, String MeterName, String ConsumedQuantity, String ResourceRate, String ExtendedCost, String ResourceLocation, String ConsumedService, String InstanceID, String ServiceInfo1, String ServiceInfo2, String AdditionalInfo, String Tags, String StoreServiceIdentifier, String DepartmentName, String CostCenter, String UnitOfMeasure, String ResourceGroup, String ChargesBilledSeparately) {
        this.AccountOwnerId = AccountOwnerId;
        this.AccountName = AccountName;
        this.SubscriptionGuid = SubscriptionGuid;
        this.SubscriptionName = SubscriptionName;
        this.Product = Product;
        this.MeterID = MeterID;
        this.MeterCategory = MeterCategory;
        this.MeterName = MeterName;
        this.ConsumedQuantity = ConsumedQuantity;
        this.ResourceRate = ResourceRate;
        this.ExtendedCost = ExtendedCost;
        this.ResourceLocation = ResourceLocation;
        this.UnitOfMeasure = UnitOfMeasure;
        this.ResourceGroup = ResourceGroup;
    }

    public String getAccountOwnerId() {
        return AccountOwnerId;
    }

    public void setAccountOwnerId(String AccountOwnerId) {
        this.AccountOwnerId = AccountOwnerId;
    }

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String AccountName) {
        this.AccountName = AccountName;
    }

    public String getSubscriptionGuid() {
        return SubscriptionGuid;
    }

    public void setSubscriptionGuid(String SubscriptionGuid) {
        this.SubscriptionGuid = SubscriptionGuid;
    }

    public String getSubscriptionName() {
        return SubscriptionName;
    }

    public void setSubscriptionName(String SubscriptionName) {
        this.SubscriptionName = SubscriptionName;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String Product) {
        this.Product = Product;
    }

    public String getMeterID() {
        return MeterID;
    }

    public void setMeterID(String MeterID) {
        this.MeterID = MeterID;
    }

    public String getMeterCategory() {
        return MeterCategory;
    }

    public void setMeterCategory(String MeterCategory) {
        this.MeterCategory = MeterCategory;
    }

    public String getMeterName() {
        return MeterName;
    }

    public void setMeterName(String MeterName) {
        this.MeterName = MeterName;
    }

    public String getConsumedQuantity() {
        return ConsumedQuantity;
    }

    public void setConsumedQuantity(String ConsumedQuantity) {
        this.ConsumedQuantity = ConsumedQuantity;
    }

    public String getResourceRate() {
        return ResourceRate;
    }

    public void setResourceRate(String ResourceRate) {
        this.ResourceRate = ResourceRate;
    }

    public String getExtendedCost() {
        return ExtendedCost;
    }

    public void setExtendedCost(String ExtendedCost) {
        this.ExtendedCost = ExtendedCost;
    }

    public String getResourceLocation() {
        return ResourceLocation;
    }

    public void setResourceLocation(String ResourceLocation) {
        this.ResourceLocation = ResourceLocation;
    }

    public String getUnitOfMeasure() {
        return UnitOfMeasure;
    }

    public void setUnitOfMeasure(String UnitOfMeasure) {
        this.UnitOfMeasure = UnitOfMeasure;
    }

    public String getResourceGroup() {
        return ResourceGroup;
    }

    public void setResourceGroup(String ResourceGroup) {
        this.ResourceGroup = ResourceGroup;
    }

    @Override
    public String toString() {
        return "{" +
            " AccountOwnerId='" + getAccountOwnerId() + "'" +
            ", AccountName='" + getAccountName() + "'" +
            ", SubscriptionGuid='" + getSubscriptionGuid() + "'" +
            ", SubscriptionName='" + getSubscriptionName() + "'" +
            ", Product='" + getProduct() + "'" +
            ", MeterID='" + getMeterID() + "'" +
            ", MeterCategory='" + getMeterCategory() + "'" +
            ", MeterName='" + getMeterName() + "'" +
            ", ConsumedQuantity='" + getConsumedQuantity() + "'" +
            ", ResourceRate='" + getResourceRate() + "'" +
            ", ExtendedCost='" + getExtendedCost() + "'" +
            ", ResourceLocation='" + getResourceLocation() + "'" +
            ", UnitOfMeasure='" + getUnitOfMeasure() + "'" +
            ", ResourceGroup='" + getResourceGroup() + "'" +
            "}";
    }
}
