package com.linkarise.batch.model.batch;

import java.util.List;

import com.linkarise.batch.model.GeneralJobRequest;

public class JobRequest extends GeneralJobRequest {
    private List<JobDetail> jobDetails;

	public List<JobDetail> getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(List<JobDetail> jobDetails) {
		this.jobDetails = jobDetails;
	}
}
