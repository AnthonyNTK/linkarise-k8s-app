package com.linkarise.batch.model;

import com.linkarise.linkarisecore.general.model.Response;

public class GeneralJobResponse extends Response {
	private JobType type;

	public GeneralJobResponse(JobType type) {
		this.setType(type);
	}

	public JobType getType() {
		return type;
	}

	public void setType(JobType type) {
		this.type = type;
	}
}
