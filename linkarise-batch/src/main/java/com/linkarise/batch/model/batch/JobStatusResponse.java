package com.linkarise.batch.model.batch;

import org.springframework.batch.core.ExitStatus;

import com.linkarise.batch.model.GeneralJobResponse;
import com.linkarise.batch.model.JobType;

public class JobStatusResponse extends GeneralJobResponse {
    private String jobId;
    private ExitStatus jobStatus;
    
    public JobStatusResponse() {
    	super(JobType.USAGE_DATA);
    }

    public JobStatusResponse(String jobId, ExitStatus jobStatus) {
    	super(JobType.USAGE_DATA);

        this.jobId = jobId;
        this.jobStatus = jobStatus;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public ExitStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(ExitStatus jobStatus) {
        this.jobStatus = jobStatus;
    }
}
