package com.linkarise.batch.model.crawl;

import com.linkarise.batch.model.GeneralJobResponse;
import com.linkarise.batch.model.JobType;

public class CrawlStatusResponse extends GeneralJobResponse {
	private String crawlId;
	private CrawlStatus crawlStatus;
	
	public CrawlStatusResponse() {
		super(JobType.STACKOVERFLOW_CRAWL);
	}

	public CrawlStatusResponse(String crawlId, CrawlStatus crawlStatus) {
		super(JobType.STACKOVERFLOW_CRAWL);

		this.crawlId = crawlId;
		this.crawlStatus = crawlStatus;
	}

	public String getCrawlId() {
		return crawlId;
	}

	public void setCrawlId(String crawlId) {
		this.crawlId = crawlId;
	}

	public CrawlStatus getCrawlStatus() {
		return crawlStatus;
	}

	public void setCrawlStatus(CrawlStatus crawlStatus) {
		this.crawlStatus = crawlStatus;
	}

}
