package com.linkarise.batch.model.batch;

import java.util.ArrayList;
import java.util.List;

import com.linkarise.batch.model.GeneralJobResponse;
import com.linkarise.batch.model.JobType;

public class JobResponse extends GeneralJobResponse {
	private List<String> executedJob;
	private List<String> savedJob;
	private List<String> notExecutedJob;

	public JobResponse() {
		super(JobType.USAGE_DATA);

		executedJob = new ArrayList<String>();
		savedJob = new ArrayList<String>();
		notExecutedJob = new ArrayList<String>();
	}

	public List<String> getExecutedJob() {
		return executedJob;
	}

	public void setExecutedJob(List<String> executedJob) {
		this.executedJob = executedJob;
	}

	public List<String> getSavedJob() {
		return savedJob;
	}

	public void setSavedJob(List<String> savedJob) {
		this.savedJob = savedJob;
	}

	public List<String> getNotExecutedJob() {
		return notExecutedJob;
	}

	public void setNotExecutedJob(List<String> notExecutedJob) {
		this.notExecutedJob = notExecutedJob;
	}

	public void addExecutedJobId(String jobId) {
		executedJob.add(jobId);
	}

	public void addSavedJobId(String jobId) {
		savedJob.add(jobId);
	}

	public void addNotExecutedJobId(String jobId) {
		notExecutedJob.add(jobId);
	}
}
