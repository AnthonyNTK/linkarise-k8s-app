package com.linkarise.batch.model;

public class GeneralJobRequest {
	private boolean execute;

	public GeneralJobRequest() {
		this.execute = false;
	}

	public GeneralJobRequest(boolean execute) {
		this.execute = execute;
	}

	public boolean isExecute() {
		return execute;
	}

	public void setExecute(boolean execute) {
		this.execute = execute;
	}
}
