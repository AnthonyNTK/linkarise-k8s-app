package com.linkarise.batch.model;

public class StockPrice {
	private String date;
	private String high;
	private String low;
	private String volume;
	private String open;
	private String close;
	private String uHigh;
	private String uLow;
	private String uVolume;
	private String uOpen;
	private String uClose;
	private String changeOverTime;
	private String label;
	private String change;
	private String changePercent;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getClose() {
		return close;
	}

	public void setClose(String close) {
		this.close = close;
	}

	public String getuHigh() {
		return uHigh;
	}

	public void setuHigh(String uHigh) {
		this.uHigh = uHigh;
	}

	public String getuLow() {
		return uLow;
	}

	public void setuLow(String uLow) {
		this.uLow = uLow;
	}

	public String getuVolume() {
		return uVolume;
	}

	public void setuVolume(String uVolume) {
		this.uVolume = uVolume;
	}

	public String getuOpen() {
		return uOpen;
	}

	public void setuOpen(String uOpen) {
		this.uOpen = uOpen;
	}

	public String getuClose() {
		return uClose;
	}

	public void setuClose(String uClose) {
		this.uClose = uClose;
	}

	public String getChangeOverTime() {
		return changeOverTime;
	}

	public void setChangeOverTime(String changeOverTime) {
		this.changeOverTime = changeOverTime;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String getChangePercent() {
		return changePercent;
	}

	public void setChangePercent(String changePercent) {
		this.changePercent = changePercent;
	}
}
