package com.linkarise.batch.model.crawl;

import java.util.List;

import com.linkarise.batch.model.GeneralJobRequest;

public class CrawlRequest extends GeneralJobRequest {
	private List<CrawlDetail> crawlDetails;

	public List<CrawlDetail> getCrawlDetails() {
		return crawlDetails;
	}

	public void setCrawlDetails(List<CrawlDetail> crawlDetails) {
		this.crawlDetails = crawlDetails;
	}
}
