package com.linkarise.batch.model.crawl;

public class CrawlStatus {
	public static final CrawlStatus EXECUTING = new CrawlStatus("EXECUTING", true);

	public static final CrawlStatus STOPPED = new CrawlStatus("STOPPED");

	public static final CrawlStatus COMPLETED = new CrawlStatus("COMPLETED");

	public static final CrawlStatus FAILED = new CrawlStatus("FAILED");

	private String exitCode;
	private String exitDescription;

	private boolean isRunning;

	public CrawlStatus(String exitCode) {
		this(exitCode, false);
	}

	public CrawlStatus(String exitCode, boolean isRunning) {
		this(exitCode, "", isRunning);
	}

	public CrawlStatus(String exitCode, String exitDescription, boolean isRunning) {
		this.exitCode = exitCode;
		this.exitDescription = exitDescription;
		this.isRunning = isRunning;
	}

	public String getExitCode() {
		return exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	public String getExitDescription() {
		return exitDescription;
	}

	public void setExitDescription(String exitDescription) {
		this.exitDescription = exitDescription;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

}
