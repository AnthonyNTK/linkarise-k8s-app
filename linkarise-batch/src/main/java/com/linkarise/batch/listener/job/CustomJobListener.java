package com.linkarise.batch.listener.job;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

public class CustomJobListener extends JobExecutionListenerSupport {
    @Override
	public void afterJob(JobExecution jobExecution) {
		System.out.println("After Job Status: " + jobExecution.getStatus());
	}

    @Override
	public void beforeJob(JobExecution jobExecution) {
    	System.out.println("Before Job Status: " + jobExecution.getStatus());
	}
}
