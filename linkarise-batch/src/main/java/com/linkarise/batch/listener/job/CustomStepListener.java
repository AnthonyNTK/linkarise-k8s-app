package com.linkarise.batch.listener.job;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.linkarise.batch.service.impl.common.ExecutorService;

public class CustomStepListener implements StepExecutionListener {
	private ExecutorService executorService;

	public CustomStepListener(ExecutorService executorService) {
		this.executorService = executorService;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		System.out.println("StepExecutionListener - beforeStep");

		executorService.upsertJobStatus(stepExecution.getJobParameters().getString("JobID"), ExitStatus.EXECUTING);
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		System.out.println("StepExecutionListener - afterStep");

		executorService.upsertJobStatus(stepExecution.getJobParameters().getString("JobID"), ExitStatus.COMPLETED);

		return ExitStatus.COMPLETED;
	}

}