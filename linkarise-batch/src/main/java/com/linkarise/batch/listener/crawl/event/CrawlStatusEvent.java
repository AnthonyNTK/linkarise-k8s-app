package com.linkarise.batch.listener.crawl.event;

import org.springframework.context.ApplicationEvent;

import com.linkarise.batch.model.crawl.CrawlStatus;

public class CrawlStatusEvent extends ApplicationEvent {
	private static final long serialVersionUID = 1L;

	private String crawlId;

	private CrawlStatus crawlStatus;
	
	public CrawlStatusEvent(String crawlId, CrawlStatus crawlStatus, Object source) {
		super(source);
		
		this.crawlId = crawlId;
		this.crawlStatus = crawlStatus;
	}

	public String getCrawlId() {
		return crawlId;
	}

	public void setCrawlId(String crawlId) {
		this.crawlId = crawlId;
	}

	public CrawlStatus getCrawlStatus() {
		return crawlStatus;
	}

	public void setCrawlStatus(CrawlStatus crawlStatus) {
		this.crawlStatus = crawlStatus;
	}
	
	
}
