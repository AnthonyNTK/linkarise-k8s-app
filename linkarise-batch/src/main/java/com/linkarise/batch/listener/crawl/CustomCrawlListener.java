package com.linkarise.batch.listener.crawl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.linkarise.batch.listener.crawl.event.CrawlStatusEvent;
import com.linkarise.batch.service.impl.common.ExecutorService;

@Component
public class CustomCrawlListener {
	@Autowired
	private ExecutorService executorService;

	@EventListener
	public void onCrawlStatusChange(CrawlStatusEvent event) {
		executorService.upsertCrawlStatus(event.getCrawlId(), event.getCrawlStatus());
	}
}
