package com.linkarise.batch.processor;

import org.springframework.batch.item.ItemProcessor;

import com.linkarise.batch.model.batch.UsageData;

public class UsageDataProcessor implements ItemProcessor<UsageData, UsageData> {
    @Override
    public UsageData process(final UsageData usageData) throws Exception {
        return usageData;
    }
}
