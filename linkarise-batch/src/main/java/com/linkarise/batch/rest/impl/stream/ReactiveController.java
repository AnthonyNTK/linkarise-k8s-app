package com.linkarise.batch.rest.impl.stream;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.linkarise.batch.model.StockPrice;
import com.linkarise.batch.service.impl.common.ReactiveService;

import reactor.core.Disposable;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/batch/reactive")
public class ReactiveController {
	@Autowired
	private ReactiveService reactiveService;

	@RequestMapping(value = "/stock/{symbol}/date/{yyyymmdd}", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public SseEmitter getStockPrice(@PathVariable("symbol") String symbol, @PathVariable("yyyymmdd") String yyyymmdd) {
		SseEmitter emitter = new SseEmitter();

		Flux<StockPrice> prices = reactiveService.getStockPrice(symbol, yyyymmdd);
		Disposable disposable = prices.take(10).subscribe((p) -> {
			try {
				emitter.send(p);
			} catch (IOException e) {
				System.out.println("IO Exception while emitting flux element");
			}
		});
		emitter.onCompletion(() -> {
			disposable.dispose();
		});
		return emitter;
	}
}
