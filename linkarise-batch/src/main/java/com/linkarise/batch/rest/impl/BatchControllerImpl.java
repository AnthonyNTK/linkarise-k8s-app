package com.linkarise.batch.rest.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.linkarise.batch.model.batch.JobDetail;
import com.linkarise.batch.model.batch.JobRequest;
import com.linkarise.batch.model.batch.JobResponse;
import com.linkarise.batch.model.batch.JobStatusResponse;
import com.linkarise.batch.rest.ThreadController;
import com.linkarise.batch.service.impl.JobServiceImpl;
import com.linkarise.batch.service.impl.common.ExecutorService;
import com.linkarise.linkarisecore.general.controller.BaseController;
import com.linkarise.linkarisecore.general.exception.RestOperationException;
import com.linkarise.linkarisecore.general.model.Response;
import com.linkarise.linkarisecore.util.ParamUtil;

@RestController
@RequestMapping("/batch/usage")
public class BatchControllerImpl implements ThreadController<JobRequest>, BaseController<JobDetail> {
	@Autowired
	private JobServiceImpl jobService;

	@Autowired
	private ExecutorService executorService;

	@Override
	@RequestMapping(value = "/start", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JobResponse start(@RequestBody(required = true) JobRequest request) {
		request.getJobDetails().forEach((detail) -> {
			if (ParamUtil.isNullOrEmpty(detail.getJobId(), detail.getSource(), detail.getDestiantion())) {
				throw new RestOperationException("Job Id, source and destination must not be null or empty value");
			}
		});

		JobResponse response = new JobResponse();

		request.getJobDetails().forEach((detail) -> {
			Job job = jobService.createJob(detail);
			if (request.isExecute()) {
				JobParameters params = new JobParametersBuilder().addString("JobID", detail.getJobId())
						.toJobParameters();
				executorService.executeTask(job, params);

				response.addExecutedJobId(detail.getJobId());
			} else {
				jobService.saveJob(detail.getJobId(), job);

				response.addSavedJobId(detail.getJobId());
			}
		});

		return response;
	}

	@Override
	@RequestMapping(value = "/execute", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JobResponse execute(@RequestBody(required = true) List<String> jobIds) {
		JobResponse response = new JobResponse();

		jobIds.forEach((jobId) -> {
			Job job = jobService.getJob(jobId);
			JobParameters params = new JobParametersBuilder().addString("JobID", jobId).toJobParameters();
			executorService.executeTask(job, params);

			jobService.removeJob(jobId);

			response.addExecutedJobId(jobId);
		});

		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<JobStatusResponse> getStatus() {
		List<JobStatusResponse> jobStatuses = new ArrayList<JobStatusResponse>();

		Iterator<Entry<String, ExitStatus>> iterator = executorService.getJobStatusMap().entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, ExitStatus> pair = (Entry<String, ExitStatus>) iterator.next();

			JobStatusResponse jobStatus = new JobStatusResponse();
			jobStatus.setJobId(pair.getKey());
			jobStatus.setJobStatus(pair.getValue());
			jobStatuses.add(jobStatus);
		}

		return jobStatuses;
	}

	@Override
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response create(@RequestBody(required = true) JobDetail detail) {
		Job job = jobService.createJob(detail);
		jobService.saveJob(detail.getJobId(), job);
		return new Response();
	}

	@Override
	public JobDetail read(String id) {
		// No implementation
		return null;
	}

	@Override
	public JobDetail update(String id, Map<String, Object> payload) {
		// No Implementation
		return null;
	}

	@Override
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response delete(@PathVariable(value = "id", required = true) String id) {
		jobService.removeJob(id);
		return new Response();
	}
}
