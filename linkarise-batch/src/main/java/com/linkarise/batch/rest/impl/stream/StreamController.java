package com.linkarise.batch.rest.impl.stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.linkarise.batch.service.impl.common.StreamService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/batch/stream")
public class StreamController {
	@Autowired
	private StreamService streamService;

	@RequestMapping(value = "/{fileName}", method = RequestMethod.GET)
	public Mono<ResponseEntity<StreamingResponseBody>> streamVideo(
			@RequestHeader(value = "Range", required = false) String httpRangeList,
			@PathVariable("fileName") String fileName) {
		return Mono.just(streamService.prepareContent(fileName, "mp4", httpRangeList));
	}
}
