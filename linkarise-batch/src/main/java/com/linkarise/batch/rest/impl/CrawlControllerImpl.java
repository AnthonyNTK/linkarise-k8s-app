package com.linkarise.batch.rest.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.linkarise.batch.model.crawl.CrawlDetail;
import com.linkarise.batch.model.crawl.CrawlJob;
import com.linkarise.batch.model.crawl.CrawlRequest;
import com.linkarise.batch.model.crawl.CrawlResponse;
import com.linkarise.batch.model.crawl.CrawlStatus;
import com.linkarise.batch.model.crawl.CrawlStatusResponse;
import com.linkarise.batch.rest.ThreadController;
import com.linkarise.batch.service.impl.CrawlServiceImpl;
import com.linkarise.batch.service.impl.common.ExecutorService;
import com.linkarise.linkarisecore.general.controller.BaseController;
import com.linkarise.linkarisecore.general.exception.RestOperationException;
import com.linkarise.linkarisecore.general.model.Response;
import com.linkarise.linkarisecore.util.ParamUtil;

@RestController
@RequestMapping("/batch/crawl")
public class CrawlControllerImpl implements ThreadController<CrawlRequest>, BaseController<CrawlDetail> {
	@Autowired
	private CrawlServiceImpl crawlService;

	@Autowired
	private ExecutorService executorService;

	@Override
	@RequestMapping(value = "/start", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public CrawlResponse start(@RequestBody(required = true) CrawlRequest request) {
		request.getCrawlDetails().forEach((detail) -> {
			if (ParamUtil.isNullOrEmpty(detail.getUrl()) && ParamUtil.isNullOrEmpty(detail.getCrawlId(),
					detail.getTag(), detail.getSize(), detail.getLimit())) {
				throw new RestOperationException(
						"CrawlId, Tag, size and limit must not be null or empty value if Url is not provided");
			}
		});

		CrawlResponse response = new CrawlResponse();

		request.getCrawlDetails().forEach((detail) -> {
			CrawlJob job = crawlService.createJob(detail);
			if (request.isExecute()) {
				executorService.executeTask(job);

				response.addExecutedCrawlId(detail.getCrawlId());
			} else {
				crawlService.saveJob(detail.getCrawlId(), job);

				response.addSavedCrawlId(detail.getCrawlId());
			}
		});

		return response;
	}

	@Override
	@RequestMapping(value = "/execute", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public CrawlResponse execute(@RequestBody(required = true) List<String> jobIds) {
		CrawlResponse response = new CrawlResponse();

		jobIds.forEach((jobId) -> {
			CrawlJob job = crawlService.getJob(jobId);
			executorService.executeTask(job);

			crawlService.removeJob(jobId);

			response.addExecutedCrawlId(jobId);
		});

		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CrawlStatusResponse> getStatus() {
		List<CrawlStatusResponse> crawlStatuses = new ArrayList<CrawlStatusResponse>();

		Iterator<Entry<String, CrawlStatus>> iterator = executorService.getCrawlStatusMap().entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, CrawlStatus> pair = (Entry<String, CrawlStatus>) iterator.next();

			CrawlStatusResponse crawlStatus = new CrawlStatusResponse();
			crawlStatus.setCrawlId(pair.getKey());
			crawlStatus.setCrawlStatus(pair.getValue());
			crawlStatuses.add(crawlStatus);
		}

		return crawlStatuses;
	}

	@Override
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response create(@RequestBody(required = true) CrawlDetail detail) {
		CrawlJob job = crawlService.createJob(detail);
		crawlService.saveJob(detail.getCrawlId(), job);
		return new Response();
	}

	@Override
	public CrawlDetail read(String id) {
		// No implementation
		return null;
	}

	@Override
	public CrawlDetail update(String id, Map<String, Object> payload) {
		// No Implementation
		return null;
	}

	@Override
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response delete(@PathVariable(value = "id", required = true) String id) {
		crawlService.removeJob(id);
		return new Response();
	}
}
