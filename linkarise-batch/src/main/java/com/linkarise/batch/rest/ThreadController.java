package com.linkarise.batch.rest;

import java.util.List;

import com.linkarise.batch.model.GeneralJobResponse;

public interface ThreadController<Request> {
	public GeneralJobResponse start(Request request);
	public GeneralJobResponse execute(List<String> jobIds);
	public <T extends GeneralJobResponse> List<T> getStatus();
}
