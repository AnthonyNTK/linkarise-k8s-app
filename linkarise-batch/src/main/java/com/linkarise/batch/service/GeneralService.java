package com.linkarise.batch.service;

public interface GeneralService<Job, Detail> {
	public Job createJob(Detail detail);

	public void saveJob(String id, Job job);

	public void removeJob(String id);

	public Job getJob(String id);
}
