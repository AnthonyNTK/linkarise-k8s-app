package com.linkarise.batch.service.impl.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import com.linkarise.batch.model.crawl.CrawlJob;
import com.linkarise.batch.model.crawl.CrawlStatus;
import com.linkarise.batch.service.runnable.CrawlRunnable;
import com.linkarise.batch.service.runnable.JobRunnable;

@Component
public class ExecutorService {
	@Autowired
	private TaskExecutor threadPoolTaskExecutor;

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private ApplicationContext applicationContext;

	private Map<String, ExitStatus> jobStatusMap = new ConcurrentHashMap<String, ExitStatus>();
	private Map<String, CrawlStatus> crawlStatusMap = new ConcurrentHashMap<String, CrawlStatus>();

	public void executeTask(Job job, JobParameters params) {
		threadPoolTaskExecutor.execute(new JobRunnable(jobLauncher, job, params));
	}

	public void executeTask(CrawlJob job) {
		threadPoolTaskExecutor.execute(new CrawlRunnable(applicationContext, job.getId(), job.getUrl()));
	}

	public Map<String, ExitStatus> getJobStatusMap() {
		return jobStatusMap;
	}

	public Map<String, CrawlStatus> getCrawlStatusMap() {
		return crawlStatusMap;
	}

	public void upsertJobStatus(String key, ExitStatus status) {
		jobStatusMap.put(key, status);
	}

	public void upsertCrawlStatus(String key, CrawlStatus status) {
		crawlStatusMap.put(key, status);
	}
}
