package com.linkarise.batch.service.runnable;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;

public class JobRunnable implements Runnable {
    private JobLauncher jobLauncher;

    private Job job;

    private JobParameters params;

    public JobRunnable(JobLauncher jobLauncher, Job job, JobParameters params) {
        this.jobLauncher = jobLauncher;
        this.job = job;
        this.params = params;
    }

    @Override
    public void run() {
        try {
            System.out.println(job.getName() + " running");
            jobLauncher.run(job, params);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
