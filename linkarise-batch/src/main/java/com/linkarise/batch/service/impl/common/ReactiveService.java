package com.linkarise.batch.service.impl.common;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.linkarise.batch.model.StockPrice;

import reactor.core.publisher.Flux;

@Component
public class ReactiveService {
	@Autowired
	private RestTemplate restTemplate;

	public Flux<StockPrice> getStockPrice(String symbol, String yyyymmdd) {
		ResponseEntity<List<StockPrice>> entity = restTemplate.exchange(String.format(
				"https://sandbox.iexapis.com/stable/stock/%s/chart/date/%s?token=Tsk_50916f358cc54cdd964d30cd70708ff7",
				symbol, yyyymmdd), HttpMethod.GET, null, new ParameterizedTypeReference<List<StockPrice>>() {
				});
		List<StockPrice> prices = entity.getBody();
		return Flux.fromIterable(prices).delayElements(Duration.of(500, ChronoUnit.MILLIS));
	}
}
