package com.linkarise.batch.service.runnable;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import org.springframework.context.ApplicationContext;

import com.linkarise.batch.listener.crawl.event.CrawlStatusEvent;
import com.linkarise.batch.model.crawl.CrawlStatus;

public class CrawlRunnable implements Runnable {
	private ApplicationContext applicationContext;

	private String id;
	private String url;

	public CrawlRunnable(ApplicationContext applicationContext, String id, String url) {
		this.applicationContext = applicationContext;
		this.url = url;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void run() {
		applicationContext.publishEvent(new CrawlStatusEvent(id, CrawlStatus.EXECUTING, this));

		try {
			Document doc = Jsoup.connect(url).get();

			doc.body().html();

			applicationContext.publishEvent(new CrawlStatusEvent(id, CrawlStatus.COMPLETED, this));
		} catch (IOException ex) {
			applicationContext.publishEvent(new CrawlStatusEvent(id, CrawlStatus.FAILED, this));
		}
	}
}
