package com.linkarise.batch.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.linkarise.batch.model.crawl.CrawlDetail;
import com.linkarise.batch.model.crawl.CrawlJob;
import com.linkarise.batch.service.GeneralService;
import com.linkarise.linkarisecore.util.ParamUtil;

@Component
public class CrawlServiceImpl implements GeneralService<CrawlJob, CrawlDetail> {
	private static final String STACKOVERFLOW_URL = "https://stackoverflow.com/questions/tagged";

	private Map<String, CrawlJob> serviceStore = new HashMap<String, CrawlJob>();

	public void saveJob(String crawlId, CrawlJob job) {
		serviceStore.put(crawlId, job);
	}

	public void removeJob(String crawlId) {
		serviceStore.remove(crawlId);
	}

	public CrawlJob getJob(String crawlId) {
		return serviceStore.get(crawlId);
	}

	public CrawlJob createJob(CrawlDetail detail) {
		return new CrawlJob(detail.getCrawlId(), constructUrl(detail));
	}

	private String constructUrl(CrawlDetail detail) {
		if (ParamUtil.isNullOrEmpty(detail.getUrl())) {
			return STACKOVERFLOW_URL + detail.getTag() + "?tab=bounties&page=" + detail.getSize() + "&pagesize="
					+ detail.getLimit();
		}
		
		return detail.getUrl();
	}
}
