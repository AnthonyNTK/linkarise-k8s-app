package com.linkarise.batch.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.linkarise.batch.config.BatchConfig;
import com.linkarise.batch.listener.job.CustomChunkListener;
import com.linkarise.batch.listener.job.CustomJobListener;
import com.linkarise.batch.listener.job.CustomStepListener;
import com.linkarise.batch.model.batch.JobDetail;
import com.linkarise.batch.model.batch.UsageData;
import com.linkarise.batch.processor.UsageDataProcessor;
import com.linkarise.batch.service.GeneralService;
import com.linkarise.batch.service.impl.common.ExecutorService;
import com.linkarise.linkarisecore.util.CommonUtil;
import com.linkarise.linkarisecore.util.ConstantUtil;

@Component
public class JobServiceImpl implements GeneralService<Job, JobDetail> {
	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private ExecutorService executorService;

	private Map<String, Job> serviceStore = new HashMap<String, Job>();

	public Job createJob(String jobName, Step step) {
		return jobBuilderFactory.get(jobName).incrementer(new RunIdIncrementer()).start(step)
				.listener(new CustomJobListener()).build();
	}

	public Job createJob(JobDetail detail) {
		return createJob(detail.getJobName(), defaultStep(detail.getStepName(), detail.getReaderName(),
				detail.getWriterName(), detail.getSource(), detail.getDestiantion()));
	}

	public void saveJob(String jobId, Job job) {
		serviceStore.put(jobId, job);
	}

	public void removeJob(String jobId) {
		serviceStore.remove(jobId);
	}

	public Job getJob(String jobId) {
		return serviceStore.get(jobId);
	}

	public Step createStep(String stepName, int chunkSize, ItemReader<UsageData> reader, ItemWriter<UsageData> writer) {
		return stepBuilderFactory.get(stepName).<UsageData, UsageData>chunk(chunkSize).reader(reader).writer(writer)
				.processor(processor()).listener(new CustomChunkListener())
				.listener(new CustomStepListener(executorService)).build();
	}

	public Step defaultStep(String stepName, String readerName, String writerName, String source, String destination) {
		return stepBuilderFactory.get(stepName).<UsageData, UsageData>chunk(BatchConfig.CHUNK_SIZE)
				.reader(createReader(readerName, source)).writer(createWriter(writerName, destination))
				.processor(processor()).listener(new CustomChunkListener())
				.listener(new CustomStepListener(executorService)).build();
	}

	private UsageDataProcessor processor() {
		return new UsageDataProcessor();
	}

	public FlatFileItemWriter<UsageData> createWriter(String writerName, String destination) {
		Resource resource = new FileSystemResource(destination);

		FlatFileItemWriter<UsageData> writer = new FlatFileItemWriter<UsageData>();
		writer.setName(writerName);
		writer.setResource(resource);
		writer.setAppendAllowed(true);
		writer.setLineAggregator(new DelimitedLineAggregator<UsageData>() {
			{
				setDelimiter(",");
				setFieldExtractor(new BeanWrapperFieldExtractor<UsageData>() {
					{
						setNames(CommonUtil.removeStringFromArray(ConstantUtil.USAGE_DATA_HEADER,
								ConstantUtil.USAGE_DATA_IGNORE_FIELDS));
					}
				});
			}
		});
		return writer;
	}

	public FlatFileItemReader<UsageData> createReader(String readerName, String source) {
		Resource resource = new FileSystemResource(source);
		FlatFileItemReader<UsageData> reader = new FlatFileItemReader<UsageData>();
		reader.setName(readerName);
		reader.setResource(resource);
		reader.setLinesToSkip(3);
		reader.setLineMapper(new DefaultLineMapper<UsageData>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(CommonUtil.removeStringFromArray(ConstantUtil.USAGE_DATA_HEADER,
								ConstantUtil.USAGE_DATA_IGNORE_FIELDS));
						setIncludedFields(generateIncludeFields(ConstantUtil.USAGE_DATA_IGNORE_FIELDS));
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<UsageData>() {
					{
						setTargetType(UsageData.class);
					}
				});
			}
		});
		return reader;
	}

	private int[] generateIncludeFields(String[] ignoreFields) {
		List<Integer> includeFieldsList = new ArrayList<Integer>();

		List<String> ignoreFieldsList = Arrays.asList(ignoreFields);
		List<Integer> ignoreIndexList = ignoreFieldsList.stream().mapToInt((ignoreField) -> {
			return CommonUtil.getIndexFromArray(ConstantUtil.USAGE_DATA_HEADER, ignoreField);
		}).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

		for (int i = 0; i < ConstantUtil.USAGE_DATA_HEADER.length; i++) {
			if (!ignoreIndexList.contains(i))
				includeFieldsList.add(i);
		}

		int[] includeFieldsArray = new int[includeFieldsList.size()];
		for (int i = 0; i < includeFieldsList.size(); i++)
			includeFieldsArray[i] = includeFieldsList.get(i);

		return includeFieldsArray;
	}
}
